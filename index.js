import DataStore from './src/DataStore'
import ajax from './src/ajax'
import MyEvents from './src/my-events'

export default {
  DataStore,
  ajax,
  MyEvents
}
