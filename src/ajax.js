const ajax = function (method, url, data, headers = {}) {
  return window.fetch(url, {
    method: method.toUpperCase(),
    body: JSON.stringify(data),  // send it as stringified json
    credentials: ajax.credentials,  // to keep the session on the request
    headers: Object.assign(ajax.headers, headers)  // extend the headers
  }).then(res => res.ok ? res.json() : Promise.reject(res))
}

// Defaults that can be globally overwritten
ajax.credentials = 'include'
ajax.headers = {
  'csrf-token': window.csrf || '',    // only if globally set, otherwise ignored
  'Accept': 'application/json',       // receive json
  'Content-Type': 'application/json'  // send json
};

// Convenient methods
['get', 'post', 'put', 'delete'].forEach(method => {
  ajax[method] = ajax.bind(null, method)
})

export default ajax
