export default class MyEvents {
  constructor (opts = {}) {
    this.opts = opts
    this.events = {}
  }

  has (eventName) {
    return this.getHandlers(eventName).length > 0
  }

  getHandlers (eventName) {
    this.events[eventName] = this.events[eventName] || []
    return this.events[eventName]
  }

  emit (eventName, ...args) {
    if (this.opts.debig) {
      console.log('emitted : ', eventName)
    }
    if (this.getHandlers(eventName).length) {
      this.events[eventName].forEach(el => {
        el.fn.apply(el.scope, args)
      })
    }
  }

  on (eventName, fn, scope = window) {
    if (typeof fn !== 'function') {
      console.log('on error : handler must be a function!')
      if (this.opts.debug) {
        throw new Error('on error : handler must be a function!')
      }
    }
    let fns = this.getHandlers(eventName)
    if (fns.length > 10) {
      console.error('on error : cant add more than 10 handlers for %s', eventName)
      if (this.opts.debug) {
        throw new Error('on error : cant add more than 10 handlers')
      }
    }
    fns.push({fn, scope})
  }
}
