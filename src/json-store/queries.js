class Queries {
  constructor (cfg, store) {
    cfg.map((el, index) => {
      if (!el.id) {
        el.id = index
      }
      return el
    })
    this.queries = new Map()
    cfg.forEach(el => {
      this.queries.set(`$fn_{el.fn}:${el.id}`, el)
    })
    if (store) {
      this.setStore(store)
    }
    // console.log('query created : ', this.queries)
  }

  has (key) {
    return this.queries.has(key)
  }

  get (key) {
    return this.queries.get(key)
  }

  setStore (store) {
    // console.log('set store : ', store)
    this.store = store
  }

  set (id, query) {
    if (!query && typeof id === 'object') {
      query = id
      id = 'default'
    }
    query.id = id
    this.queries.set(id, query)
    this.update()
    // console.log('query set : ', this.queries)
    return this.store
  }

  switchOff (id, state) {
    let query = this.queries.get(id)
    if (query) {
      query.disabled = state
      this.set(id, query)
    }
    return this.store
  }

  on (id) {
    // console.log('query off : ', id)
    return this.switchOff(id, false)
  }

  off (id) {
    // console.log('query on : ', id)
    return this.switchOff(id, true)
  }

  normalizeFunc (fn) {
    if (!fn.id) {
      fn.id = 'default'
    }
    if (fn.values && !Array.isArray(fn.values)) {
      fn.values = [fn.values]
    }
    return fn
  }

  update () {
    let options = JSON.parse(this.store.proxy.extraParams.options)
    options.query = this.build()
    // console.log('query update : ', options.query)
    this.store.proxy.setExtraParam('options', JSON.stringify(options))
  }

  build () {
    let values = []
    this.queries.forEach((el, key) => {
      if (Array.isArray(el) && !el.disabled) {
        el.filter(el => !el.disabled).map(this.normalizeFunc).forEach(el => values.push(el))
      } else {
        el = this.normalizeFunc(el)
        if (!el.disabled) {
          values.push(el)
        }
      }
    })
    // console.log('query build : ', values)
    return values
  }
}

export default Queries
