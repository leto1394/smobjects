import MyEvents from '../my-events'
import ajax from '../ajax'
/**
 * ## JsonStore - remote json store for frontend use
 * support paging, MYSQL queryng
 */
class JsonStore extends MyEvents {
/**
 * @param {Object} opts - config
 * @param {string} opts.model - name of data table in database
 * @param {Object} opts.paging - paging params, if not defined then paging is currently disabled
 * @param {number} opts.paging.page - page number
 * @param {number} opts.paging.limit - records per page
 */
  constructor (opts = {}) {
    super()
    this.cfg = {
      model: opts.model,
      paging: opts.paging || {disabled: true}
    }
    this.ajax = ajax
    this.items = opts.items || []
  }

  page (opts) {
    if (opts.disable) {
      this._options.disablePagination = true
    } else {
      this._options.page = opts.page
      this._options.limit = opts.limit || 25
    }
    return this
  }
/**
 * query interface
 */
  query () {
  }

  load () {
    let opts = {
      table: this.model,
      options: this._options
    }
    this.ajax.post('/api/json/read', opts)
    .then(data => {
      console.log('data = ', data)
    })
    .catch(data => {

    })
  }
}

export default JsonStore
