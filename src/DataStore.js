export default class DataStore {
  constructor (opts, missCB) {
    const data = opts
    this.getData = () => data
    if (typeof missCB === 'undefined') {
      missCB = function (args) { console.error('DataStore ::: missed path : ', args.join(' , ')) }
    }
    if (typeof missCB === 'function') {
      this.miss(missCB)
    }
  }
// set callback for given props path is missed
  miss (callback, scope) {
    if (callback && typeof callback === 'function') {
      this._missCB = callback.bind(scope)
    } else {
      this._missCB = undefined
    }
  }

  getProp (data, prop, cb) {
    if (data.hasOwnProperty(prop)) {
      cb.call(this, data[prop])
      return true
    }
    return false
  }

  get (...args) {
    let cursor = this.getData()
    args.find(arg => {
      if (arg.$options && arg.$options.className) {
        arg = arg.$options.className
      }
      if (this.getProp(cursor, arg, function (data) { cursor = data })) {
        return false
      } else {
        if (this._missCB) {
          this._missCB(args)
        }
        cursor = false
        return true
      }
    })
    return cursor
  }

  set (...args) {
    let value = args.pop()
    let lastArg, lastCursor
    let cursor = this.getData()
    args.find(arg => {
      if (arg.$options && arg.$options.className) {
        arg = arg.$options.className
      }
      lastArg = arg
      lastCursor = cursor
      if (this.getProp(cursor, arg, function (data) { cursor = data })) {
        return false
      } else {
        if (this._missCB) {
          this._missCB(args)
        }
        cursor = false
        return true
      }
    })
    if (cursor && lastCursor) {
      lastCursor[lastArg] = value
      return lastCursor
    }
  }
}
